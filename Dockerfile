FROM python:3
ADD ./requirements.txt /requirements.txt
WORKDIR /usr/src/app
EXPOSE 4000
RUN pip install --upgrade pip
RUN pip install -r /requirements.txt
ENTRYPOINT ["python","index.py"]