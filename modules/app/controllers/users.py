''' controller and routes for users '''
import os
from flask import request, jsonify
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, jwt_refresh_token_required, get_jwt_identity)

from app import app, mongo, flask_bcrypt, jwt
from app.schemas.user import validate_user
import logger
from bson.objectid import ObjectId
import tools
from app.decorators import (auth_required, admin_required)

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@app.route('/auth', methods=['POST'])
def auth_user():
    ''' auth endpoint '''
    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        users = list(mongo.db.users.find())
        if not users:
            if os.environ.get('ADMIN_USERNAME') and os.environ.get('ADMIN_PASSWORD'):
                LOG.info("No users in database, creating default user... username: %s password: %s" % (os.environ.get('ADMIN_USERNAME'),os.environ.get('ADMIN_PASSWORD')))
                data = {
                    'username': os.environ.get('ADMIN_USERNAME'),
                    'password': flask_bcrypt.generate_password_hash(os.environ.get('ADMIN_PASSWORD')),
                    'name': 'admin',
                    'email': 'admin@example.com'
                }
                mongo.db.users.insert_one(data)
                LOG.info("User created")
            else:
                LOG.info("No env vars ADMIN_USERNAME, ADMIN_PASSWORD for init user")
        user = mongo.db.users.find_one({'username': data['username']}, {"_id": 0})
        if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
            del user['password']
            data['role']= "admin" # get role from db
            access_token = create_access_token(identity=data)
            refresh_token = create_refresh_token(identity=data)
            user['token'] = access_token
            user['refresh'] = refresh_token
            return jsonify({'ok': True, 'data': user}), 200
        else:
            return jsonify({'ok': False, 'message': 'invalid username or password'}), 401
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/register', methods=['POST'])
# @admin_required ## remove if you want to enable user regsiter
def register():
    ''' register user endpoint '''
    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        data['password'] = flask_bcrypt.generate_password_hash(
            data['password'])
        mongo.db.users.insert_one(data)
        return tools.response({},200,"User created")
    else:
        return tools.response({},400,format(data['message']))


@app.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    ''' refresh token endpoint '''
    current_user = get_jwt_identity()
    ret = {
        'token': create_access_token(identity=current_user)
    }
    return jsonify({'ok': True, 'data': ret}), 200


@app.route('/user', methods=['GET', 'POST', 'DELETE', 'PATCH'])

def user():
    if request.method == 'GET':
        query = request.args
        data = list(mongo.db.users.find({},{ "_id": 1, "name": 1, "email": 1 }))
        return tools.response(data)

    data = request.get_json()
    if request.method == 'POST':
        if data.get('name', None) is not None and data.get('email', None) is not None:
            mongo.db.users.insert_one(data)
            return tools.response(data)
        else:
            return tools.response(data,400,"Params name and email are required")



@app.route('/user/<user_id>', methods=['GET', 'POST', 'DELETE', 'PATCH'])
@jwt_required
def user_actions(user_id):
    if request.method == 'GET':
        query = {
            "_id": ObjectId(user_id)
        }
        data = mongo.db.users.find_one(query)
        if data:
            return tools.response(data)
        else:
            return tools.response(data,404)
    data = request.get_json()
    if request.method == 'DELETE':
        db_response = mongo.db.users.delete_one({"_id":ObjectId(user_id)})
        if db_response.deleted_count == 1:
            return tools.response(data,200,"record deleted")
        else:
            return tools.response(data,404,"record not found")

    if request.method == 'PATCH':
        print(data.get('payload', {}))
        mongo.db.users.update_one({"_id":ObjectId(user_id)}, {'$set': data})
        return tools.response({},200,"record updated")