from functools import wraps
from flask import g, request, redirect, url_for, abort
from flask_jwt_extended import (jwt_required, get_jwt_identity,get_jwt_claims)
import tools

def admin_required(f):
    @wraps(f)
    @jwt_required
    def decorated_function(*args, **kwargs):
        current_user = get_jwt_identity()
        if current_user['role'] != 'admin':
            return tools.response({},403)
        return f(*args, **kwargs)
    return decorated_function


def auth_required(f):
    @wraps(f)
    @jwt_required
    def decorated_function(*args, **kwargs):
        current_user = get_jwt_identity()
        if current_user['role']:
            return tools.response({},401)
        return f(*args, **kwargs)
    return decorated_function