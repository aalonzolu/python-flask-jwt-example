''' response data rest '''
from flask import jsonify

def response(data, status=200, message=False):
    if message == False:
        if status == 200:
            message = "Success"
        elif status==404:
            message = "Not Found"
        elif status==400:
            message = "Bad Request"
        elif status==401:
            message = "Unauthorized"
        elif status==403:
            message = "Forbidden"
        elif status==500:
            message = "Internal server error"
        elif status==502:
            message = "Bad Gateway"

    if data is not None:
        return jsonify({
            'status': status,
            'message': message,
            'data': data
        }), status
    else:
        return jsonify({
            'status': status,
            'message': message,
            'data': {},
        }), status
