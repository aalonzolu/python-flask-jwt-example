import Vue from 'vue'
Vue.mixin({
  methods: {
    sayHello: (name)=>{
        return `Hello ${name}`
    },
    ifShowMenu: (route)=>{
        return !["/login","/register","/"].includes(route)
    }
  }
})